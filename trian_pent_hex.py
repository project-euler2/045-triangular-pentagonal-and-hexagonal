import time
start_time = time.time()

def find_tri_pent_hex(limit):
    pentagonal_numbers = [int(x*(3*x-1)*0.5) for x in list(range(1,limit))]
    hexagonal_numbers = [int(x*(2*x-1)) for x in list(range(1,limit))]

    return list(set(pentagonal_numbers).intersection(hexagonal_numbers))

print(find_tri_pent_hex(70000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )